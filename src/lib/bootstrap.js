import $ from 'jquery';
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';

export function bootstrap() {
	var ws = new SockJS('http://172.20.0.17:15674/stomp');
	ws.onopen = function() {
	    console.log('open');
	};
	ws.onclose = function() {
	    console.log('close');
	};

	var printSub = function(message) {
		console.log(message.headers.destination, ':', message.body);
	}

	window.client = Stomp.over(ws);
	window.client.debug = null;
	var headers = {
			login: 'client',
			passcode: 'client',
			host: 'radioplay'
		};
	window.client.connect(
		headers,
		function(a,b){
			window.client.subscribe('/exchange/onair/test1', printSub);
			//window.client.subscribe('/exchange/onair/test3', printSub);
			//console.log('connect failed?', a, b);
		}
	);
}


